//
//  DirectorySelector.swift
//  MagMac
//
//  Created by john on 6/14/18.
//  Copyright © 2018 john. All rights reserved.
//

import Cocoa



/// The user selected directory type.
///
/// - input: Input directory selection.
/// - output: Output directory selection.

enum DirectorySelection {
    case input
    case output
}


/// The DirectorySelectionDelegate notifies its delegate of a directory selection.

protocol DirectorySelectionDelegate {
    
    /// A directory selection has occurred.
    ///
    /// - Parameters:
    ///   - directory: The directory that was chosen.
    ///   - selection: The selection: either .input or .output
    
    func didSelect(directory: URL, for selection: DirectorySelection)
}


/// The DirectorySelector class presents an NSOpenPanel in order to allow
/// the selection of an input and an output directory for mag and thumb file generation.

class DirectorySelector {
    
    
    /// Presents the NSOpenPanel for input/output directory selection.
    ///
    /// - Parameters:
    ///   - selection: Which type of directory was selected.
    ///   - window: The window to present the panel in.
    ///   - delegate: The delegate to be notified of the selection.
    
    class func present(for selection: DirectorySelection, window: NSWindow, delegate: DirectorySelectionDelegate) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseFiles = false
        openPanel.canChooseDirectories = true
        openPanel.canCreateDirectories = true
        
        openPanel.beginSheetModal(for: window) { (response) in
            if let url = openPanel.url, response == .OK {
                delegate.didSelect(directory: url, for: selection)
            }
        }
    }
}
