//
//  ViewController.swift
//  MagMac
//
//  Created by john on 6/13/18.
//  Copyright © 2018 john. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, ViewPresentable {
    
    //The button that launches the generation process.
    @IBOutlet weak var generateButton: NSButtonCell!
    
    //The viewModel associated with this controller.
    var viewModel: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ViewModel()
        viewModel.delegate = self
    }
    
    @IBAction func inputDirButtonPressed(_ sender: NSButton) {
        DirectorySelector.present(for: .input, window: view.window!, delegate: viewModel)
    }
    
    @IBAction func outputDirButtonPressed(_ sender: NSButtonCell) {
        DirectorySelector.present(for: .output, window: view.window!, delegate: viewModel)
    }
    
    @IBAction func generateButtonPressed(_ sender: NSButton) {
        viewModel.generateFiles()
    }

    func shouldEnableGeneration(shouldEnable: Bool) {
        generateButton.isEnabled = shouldEnable
    }
    
    func generationCompleted(title: String, message: String) {
        let alert = NSAlert()
        alert.informativeText = message
        alert.messageText = title
        alert.beginSheetModal(for: view.window!, completionHandler: nil)
    }
}

