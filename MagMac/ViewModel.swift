//
//  ViewModel.swift
//  MagMac
//
//  Created by john on 6/13/18.
//  Copyright © 2018 john. All rights reserved.
//

import Foundation
import MAGCommon



/// The ViewPresentable protocol is used to notify the view of
/// generation events.
@objc protocol ViewPresentable {
    
    
    /// Determines whether generation should be enabled.
    ///
    /// - Parameter shouldEnable: true if generation can be enabled, false if not.
    func shouldEnableGeneration(shouldEnable: Bool)
    
    
    /// The mag and thumb file generation has completed.
    /// There is no facility to distinguish between a successful or failed completion
    /// outside of the message itself.
    /// - Parameters:
    ///   - title: The title of the result.
    ///   - message: The message of the result.
    func generationCompleted(title: String, message: String)
}



/// The ViewModel class is a loose adaptation of the MVVM model.
/// It is responsible for coordinating directory selection and generation with it's view.

class ViewModel {
    
    // The delegate of this viewModel that conforms to the ViewPresentable protocol.
    weak var delegate: ViewPresentable?
    
    // The input directory to create mag and thumb files from.
    var inputDirectory: URL?
    
    // The output directory to put the generated mag and thumb files into.
    var outputDirectory: URL?
    
    
    /// Convenience method to determine whether generation can be enabled.
    /// - Returns: true if generation can be enabled, fals if not.
    
    func canEnableGeneration() -> Bool {
        return inputDirectory != nil && outputDirectory != nil ? true : false
    }
    
    /// Generates the mag and thumb files.
    func generateFiles() {
        let magWriter = MAGWriter(inputPath: inputDirectory!.path, outputPath: outputDirectory!.path)
        do {
            try magWriter.processFiles()
            delegate?.generationCompleted(title: "Generation complete!", message: "The mag and thumb files have been generated.")
        } catch {
            let message = "There was an error doing generation. The cause was: \(error.localizedDescription)"
            delegate?.generationCompleted(title: "Generation failed!", message: message)
        }
    }
}

//MARK: DirectorySelectionDelegate methods

extension ViewModel: DirectorySelectionDelegate {
    func didSelect(directory: URL, for selection: DirectorySelection) {
        if selection == .input {
            inputDirectory = directory
        } else {
            outputDirectory = directory
        }
        delegate?.shouldEnableGeneration(shouldEnable: canEnableGeneration())
    }
}
